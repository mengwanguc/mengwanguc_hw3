#include <iostream>

using namespace std;

class ForceInitialization {
public:
	ForceInitialization() { ios_base::Init(); }
};

ForceInitialization force;

class HelloWorld {
public:
	HelloWorld() { cout << "Hello, World!" << endl; }
};

HelloWorld hello;

int main() {
	return 0;
}