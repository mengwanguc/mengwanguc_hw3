#include <iostream>

using namespace std;

class B {
public:
	B() {}
	virtual void f() {};
};

class D : public B {
public:
	D() {}
};


int main() {
	B *b = new B;
	cout << "dynamic cast:\t" << dynamic_cast<D*>(b) << endl;;
	cout << "static  cast:\t" << static_cast<D*>(b) << endl;
	cout << "C-style cast:\t" << (D*)b << endl;
}

