#include <iostream>
#include <string>

using namespace std;

class TreeNode {
public:
	bool isAnimal;		// whether the node represents an animal or a question
	string animal;
	string question;
	TreeNode *left;		// turn left if the answer to the question is true
	TreeNode *right;	// turn right if the answer is false

	TreeNode(string animal): isAnimal(true), animal(animal), question(""),left(NULL), right(NULL) {}
	void setQuestion(string question, string newAnimal, bool newAnimalCanDoIt) {
		isAnimal = false;
		this->question = question;
		if (newAnimalCanDoIt) {
			this->left = new TreeNode(newAnimal);
			this->right = new TreeNode(animal);
		}
		else {
			this->left = new TreeNode(animal);
			this->right = new TreeNode(newAnimal);
		}
		animal = "";
	}

	// deconstruct the class recursively
	~TreeNode() {
		if (!isAnimal) {
			delete(this->left);
			delete(this->right);
		}
	}
};


int main() {
	TreeNode *root = new TreeNode("cat");
	string input = "";
	while (true) {
		cout << "Choose a secret animal."<< endl;
		TreeNode *tempNode = root;

		// keep asking the question
		while (!tempNode->isAnimal && input != "quit") {
			cout << tempNode->question << endl;
			cin >> input;
			if (input == "yes")
				tempNode = tempNode->left;
			else
				tempNode = tempNode->right;
		}
		cout << "Is it a " << tempNode->animal << " ? (print \"yes\ or \"no\")" << endl;
		cin >> input;

		// if the answer is wrong
		if (input == "no") {
			string newAnimal, question;
			cout << "You Win!" << endl << "Plese tell me the animal you choose: " << endl;
			char temp;
			cin >> temp;
			getline(cin, newAnimal);
			newAnimal = temp + newAnimal;

			cout << "Please give a differentiating yes/no question for " << tempNode->animal << " and " << newAnimal << endl;
			cin >> temp;
			getline(cin, question);
			question = temp + question;
			cout << "For " << newAnimal << " , " << question << endl;
			cin >> input;
			if (input == "yes")
				tempNode->setQuestion(question, newAnimal, true);
			else
				tempNode->setQuestion(question, newAnimal, false);
		}

		cout << "Do you want to play it again?" << endl;
		cin >> input;
		if (input == "no")
			break;
	}
	delete(root);
}