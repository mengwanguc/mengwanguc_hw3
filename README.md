### What is this repository for? ###

This is my solutions to homework 3 in the course C++.


### hw 3.1 ###

3.1.cpp is the source code of my solution to hw 3.1. 


### hw 3.2 ###

3.2.cpp is the source code of my solution to hw 3.2.
We didn't use "new" to allocate memory for i, so we cannot delete &i.
ip is an array of integers, So we need to use delete[] to destroy the array.


### hw 3.3 ###

3.3.cpp is the source code of my solution to hw 3.3.
As we see, when using dynamic_cast, we return a pointer 0. That's because B* is not a D*.
However for C-style behavior, it return the address of b, implying it has already casted B* to D*.

I think dynamic_cast is better, because the C-style behavior is unsafe, There may be some elements in D that do not exist in B. When we call these elements, it will cause errors.

To get the C-style behavior, we can use static_cast.



### hw 3.4 ###

We always need to read it backwards to get its meaning.

For example, "int const*" means a pointer of a constant integer. "const int*" also means a  pointer of a constant integer. But "int* const" means a const pointer of an integer, actually the different thing.

I think "const int" is better, because it won't cause much confusion. We just need to put const before what we want to be constant. 


### hw 3.5 ###

3.5.cpp is the source code of my solution to hw 3.5.